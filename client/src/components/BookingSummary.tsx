import React from 'react';
import './BookingSummary.css';

const BookingSummary: React.FC = () => {
    return (
        <div id='booking-summary'>
            <p>Booking Code: A52SDE</p>
            <p>Name: Adnan Gazi</p>
            <p>Home: Wellness crescent</p>
            <p>City: London</p>
        </div>);
}

export default BookingSummary;