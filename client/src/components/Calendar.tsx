import React, { useState } from 'react';
import './Calendar.css';

interface ICaledarProps {
    startingMonth: number
}

const Calendar: React.FC<ICaledarProps> = ({ startingMonth }) => {
    const [year, setYear] = useState(2019);
    const [month, setMonth] = useState(startingMonth);

    const getWeekdayFromNextMonth = (n?: number) => {
        return new Date(year, month + 1, n);
    }
    const getWeekdayFromMonth = (n?: number) => {
        return new Date(year, month, n);
    }
    const [selected, setSelected] = useState(1);

    var date = getWeekdayFromNextMonth(0);
    const monthName = date.toLocaleString('default', { month: 'long' })
    const totalDays = getWeekdayFromNextMonth(0).getDate();
    const start = getWeekdayFromMonth(8).getDay();

    if (selected > totalDays) {
        setSelected(totalDays);
    }

    let day = [];
    let rows = [];

    for (let i = 1; i <= 35; i++) {
        if (i > start && (i - start) <= totalDays) {
            day.push(i - start);
        } else {
            day.push(0);
        }
        if (i % 7 === 0) {
            rows.push(day);
            day = [];
        }
    }

    const handleClick = (n: number) => {
        if (n > 0) setSelected(n);
    }

    const changeMonth = (n: number) => {
        if (n < 0) {
            setMonth(11);
            setYear(year - 1);
        } else if (n > 11) {
            setMonth(0);
            setYear(year + 1);
        } else {
            setMonth(n)
        };
    }

    let dayNames = ['Su', 'Mo', ' Tu', 'We', 'Th', 'Fr', 'Sa']

    return (
        <div id="calendar">
            <div className='calendarHeader'>
                <div className='monthLeft' onClick={() => changeMonth(month - 1)}>-</div>
                <span className='shortDate'>{year}/{month + 1} {monthName}, {selected}/{totalDays}</span>
                <div className='monthRight' onClick={() => changeMonth(month + 1)}>+</div>
            </div>
            <div className='calendarRow'>
                {dayNames.map(d => (<div className='cell dayName'>{d}</div>))}
            </div>
            {rows.map(numbers => {
                return (
                    <div className='calendarRow'>{numbers.map(n => {
                        if (n === 0) {
                            return <div className='cell none'></div>
                        }
                        let className = 'cell date';

                        className += n !== selected ? '' : ' clicked';
                        return (
                            <div className={className} onClick={() => handleClick(n)}>{n}</div>
                        )
                    })}
                    </div>)
            })}
        </div>);
}

export default Calendar;