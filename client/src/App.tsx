import React from 'react';
import Calendar from './components/Calendar';
import BookingSummary from './components/BookingSummary';

const App: React.FC = () => {

  return (
    <div className="App">
      <BookingSummary />
      <Calendar startingMonth={8} />
    </div>
  );
}

export default App;
